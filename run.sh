#!/bin/zsh

zparseopts -E -D d:=day y:=year i:=input

if [[ -z "${day}" ]]
then
    echo "-d argument is missing"
    exit 1
elif ! [[ "${day[-1]}" =~ ^[0-9]{1,2}$ ]]
then
    echo "-d is not a valid number"
    exit 1
else
    day="${day[-1]}"
fi

if [[ -z "${year}" ]]
then
    echo "-y argument is missing"
    exit 1
elif ! [[ "${year[-1]}" =~ ^[0-9]{4}$ ]]
then
    echo "-y is not a valid number"
    exit 1
else
    year="${year[-1]}"
fi

input="${input[-1]}"
cd ${year}/${day} && python3 main.py "${input}"
