# Advent of Code

My contribution to the [Advent of Code](https://adventofcode.com/).

I'm starting this challenge in May, for the advent was last December. Which proves how serious I am about it, just want to give it a try. Will do in python.

Thanks to [Yonatan Ramirez](https://gitlab.com/yonatan.ramirez/advent-of-code) whom introduced me to the Advent of Code and kindly let me borrow some code.

## Get the input data

In order to get the input data and save it into the correct folder, run the following command:
```bash
./get_input.sh -d day -y year
```

where `day` and `year` are the **day** (without leading 0) and **year** of the challenge, respectively.

You will need to put your **session** cookie into a `cookie.txt` file, at the the same location as this script. You can get this cookie from your web browser (right click -> inspect element -> Application tab -> Storage -> Cookies -> session) after authenticating into the advent of code site.

## Execute a challenge

You can easily run a challenge using the following command
```bash
./run.sh -d day -y year -i input
```
where `day` and `year` are the **day** (without leading 0) and **year** of the challenge, and `input` is the **input** file.
