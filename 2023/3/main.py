import argparse
import re


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return data


def parse_schematic(data, pattern):
    numbers = []
    symbols = []
    for i, line in enumerate(data):
        numbers += [(range(max(m.start()-1, 0), min(m.end()+1, len(data))), range(max(i-1, 0), min(i+2, len(data))), m.group(0)) for m in re.finditer(r'\d+', line)]  # tuple (range x, range y)
        symbols += [(m.start(), i) for m in re.finditer(pattern, line)]  # tuple x, y
    return numbers, symbols


def is_overlap(nb, sb):
    return sb[0] in nb[0] and sb[1] in nb[1]


def main_p1(filepath):
    data = read_file(filepath)
    numbers, symbols = parse_schematic(data, pattern=r'[^.\d\n]')
    valids = []
    for nb in numbers:
        for sb in symbols:
            if is_overlap(nb, sb):
                valids.append(int(nb[2]))
                break
    print(f"Sum: {sum(valids)}")
    return


def main_p2(filepath):
    data = read_file(filepath)
    numbers, gears = parse_schematic(data, pattern=r'\*')
    gear_ratios = []
    for gr in gears:
        adj_nbs = []
        for nb in numbers:
            if is_overlap(nb, gr):
                adj_nbs.append(nb)
        if len(adj_nbs) == 2:
            gear_ratios.append(int(adj_nbs[0][2])*int(adj_nbs[1][2]))
    print(f"Sum gear ratios: {sum(gear_ratios)}")
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    print("Part 1")
    main_p1(args.filename)
    print("Part 2")
    main_p2(args.filename)
