import argparse
import regex as re


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return data


def parse_line(line):
    cline = line.split(':')[1].split('|')
    winnings = re.findall(r'\d+', cline[0])
    numbers = re.findall(r'\d+', cline[1])
    return set(winnings), set(numbers)


def main(filepath):
    data = read_file(filepath)
    score = []
    for line in data:
        winnings, numbers = parse_line(line)
        inter = winnings.intersection(numbers)
        if inter:
            score.append(2**(len(inter)-1))
    print(f"Total score: {sum(score)}")
    return


def main_p2(filepath):
    data = read_file(filepath)
    multiple_card = [1 for _ in range(len(data))]
    for i, (line, multiplier) in enumerate(zip(data, multiple_card)):
        winnings, numbers = parse_line(line)
        for _ in range(multiplier):
            inter = winnings.intersection(numbers)
            for k, _ in enumerate(inter):
                multiple_card[i+k+1] += 1
    print(f'Total number of scratchcards: {sum(multiple_card)}')
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    print("Part 1:")
    main(args.filename)
    print("Part 2:")
    main_p2(args.filename)
    
