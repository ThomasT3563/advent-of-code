import argparse
import re

def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return data


def parse_line(line):
    id = int(line[4:].split(':')[0])
    n_red = [int(m) for m in re.findall(r'\d+(?= red)', line)]
    n_green = [int(m) for m in re.findall(r'\d+(?= green)', line)]
    n_blue = [int(m) for m in re.findall(r'\d+(?= blue)', line)]
    return id, n_red, n_green, n_blue


def main_p1(filepath):
    limit = {
        'red': 12,
        'green': 13,
        'blue': 14
    }
    valid_ids = []
    data = read_file(filepath)
    for line in data:
        id, n_red, n_green, n_blue = parse_line(line)
        if max(n_red) <= limit['red'] and max(n_green) <= limit['green'] and max(n_blue) <= limit['blue']:
            valid_ids.append(id)
    print(f"Sum IDs: {sum(valid_ids)}")
    return


def main_p2(filepath):
    data = read_file(filepath)
    pw_sets = []
    for line in data:
        id, n_red, n_green, n_blue = parse_line(line)
        pw_sets.append(max(n_red) * max(n_green) * max(n_blue))
    print(f"Sum sets power: {sum(pw_sets)}")
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    print("Part1:")
    main_p1(args.filename)
    print("Part2:")
    main_p2(args.filename)
