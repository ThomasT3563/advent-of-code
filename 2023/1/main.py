import argparse
import regex as re


MATCH_NB = {
    "zero": 0,
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9
}


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return data


def get_calibration_value(line):
    nbs = re.findall(r'\d', line)
    if nbs:
        return int(f'{nbs[0]}{nbs[-1]}')
    else:
        print(f'Cannot find calibration for {line}')
        return 0


def main_part1(filepath):
    data = read_file(filepath)
    cal_values = [get_calibration_value(data_line) for data_line in data]
    print(f'Calibration value: {sum(cal_values)}')
    return


def main_part2(filepath):
    data = read_file(filepath)
    cal_values = []
    for data_line in data:
        digits = []
        for i, s in enumerate(data_line):
            if s.isdigit():
                digits.append(s)
            else:
                for k in MATCH_NB:
                    if data_line[i:].startswith(k):
                        digits.append(MATCH_NB[k])
        cal_values.append(int(f'{digits[0]}{digits[-1]}'))
    print(f'Calibration value: {sum(cal_values)}')
    return
    

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    print("Part 1:")
    main_part1(args.filename)
    print("Part 2:")
    main_part2(args.filename)

