#!/bin/zsh

zparseopts -E -D d:=day y:=year

if [[ -z "${day}" ]]
then
    echo "-d argument is missing"
    exit 1
elif ! [[ "${day[-1]}" =~ ^[0-9]{1,2}$ ]]
then
    echo "-d is not a valid number"
    exit 1
else
    day="${day[-1]}"
fi

if [[ -z "${year}" ]]
then
    echo "-y argument is missing"
    exit 1
elif ! [[ "${year[-1]}" =~ ^[0-9]{4}$ ]]
then
    echo "-y is not a valid number"
    exit 1
else
    year="${year[-1]}"
fi
cookie=$(cat cookie.txt)

curl -s -f --create-dirs -o "${year}/${day}/input" -b session=${cookie} "https://adventofcode.com/${year}/day/${day}/input"

# instantiate file main.py
cat > "${year}/${day}/main.py" << EOF
import argparse


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return data


def main1(filepath):
    print("Hello world!")
    return


def main2(filepath):
    print("Hello world!")
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main1(args.filename)
    main2(args.filename)
EOF

if [[ $? == 22 ]]
then
    rmdir ${year}/${day} > /dev/null 2>&1
    rmdir ${year} > /dev/null 2>&1
    exit 2
fi

exit 0
