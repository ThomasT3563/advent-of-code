import argparse
import re
import numpy as np

from functools import reduce


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.read().split("\n")
    return [list(map(int, re.findall(r'-?\d+', l))) for l in data]


def __display(d, robots):
    print("\nmap display:\n")
    for y in range(d[1]):
        line = ""
        for x in range(d[0]):
            line += str(robots.get((x,y), "."))
        print(line)
    return


def main(data, d, t, display=False):
    "hint from community: calculate variance for part 2"
    robots = {}
    quadrants = {}
    for x,y,vx,vy in data:
        (x, y) = ((x+t*vx)%d[0], (y+t*vy)%d[1])
        robots[(x,y)] = robots.get((x,y), 0) +1
        if (x != d[0]//2) and (y != d[1]//2):
            q = int(x // (d[0]/2) + 2*(y // (d[1]/2)))
            quadrants[q] = quadrants.get(q, 0) + 1
    if display:
        __display(d, robots)
    # return score quadrants / variance of X coordinates
    return reduce(lambda x,y: x*y, list(quadrants.values())), np.var([x for (x,_), _ in robots.items()])


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    D = read_file(args.filename)
    # part 1
    score, tvar = main(D, d=(101,103), t=100)
    print(score)
    # part 2: takes ~4s
    vars = [main(D, d=(101,103), t=i)[1] for i in range(0, 9999)]
    print(vars.index(min(vars)))