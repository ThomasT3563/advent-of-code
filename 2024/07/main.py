import argparse
from itertools import product
from operator import mul, add
import re
import time


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return [[int(i)for i in re.findall(r'\d+', line)] for line in data]


###### takes 1h10 for part 1, don't do that ######

from itertools import combinations_with_replacement, permutations

def _get_operations_before(n):
    return {perm for comb in combinations_with_replacement([mul, add], n) for perm in permutations(comb, n)}

##################################################


def _get_operations(n, operations):
    return [comb for comb in product(operations, repeat=n)]


def _is_equal(eq, op):
    result =  eq[1]
    for o, n in zip(op, eq[2:]):
        result = o(result, n)
    return eq[0] == result


def main1(filepath):
    sum_valid = 0
    for eq in read_file(filepath):  # 
        for op in _get_operations(len(eq[1:])-1, [mul, add]):
            if _is_equal(eq, op):
                sum_valid += eq[0]
                break
    print(sum_valid)
    return


def concat(x,y):
    return int(str(x)+str(y))


def main2(filepath):
    sum_valid = 0
    for eq in read_file(filepath):
        for op in _get_operations(len(eq[1:])-1, [mul, add, concat]):
            if _is_equal(eq, op):
                sum_valid += eq[0]
                break
    print(sum_valid)
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    t_start = time.time()
    main1(args.filename)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
    t_start = time.time()
    main2(args.filename)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
