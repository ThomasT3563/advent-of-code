import argparse
from itertools import combinations, product
import time


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return [l.strip() for l in data]


def _get_antennas(data):
    antennas = {}
    for y, line in enumerate(data):
        for x, l in enumerate(line):
            if l != ".":
                ant = antennas.get(l, [])
                ant.append((y,x))
                antennas[l] = ant
    return antennas


def _calc_f_repeat(p1, p2, max_y, max_x, part2=False):
    f_antinodes = []
    ay = 2*p1[0]-p2[0]
    ax = 2*p1[1]-p2[1]
    while (0 <= ay < max_y) and (0 <= ax < max_x):
        f_antinodes.append((ay, ax))
        if part2 is False:  # stops at 1 antinode frequency
            return [(ay, ax)]
        # calc next T-frequency
        ay += p1[0]-p2[0]
        ax += p1[1]-p2[1]
    return f_antinodes


def _get_antinodes(antennas, max_y, max_x, part2=False):
    antinodes = []
    for t_ants in antennas.values():
        for (p1, p2) in combinations(t_ants, 2):
            if part2:  # includes antenna points
                antinodes += [p1, p2]
            antinodes += _calc_f_repeat(p1, p2, max_y, max_x, part2)
            antinodes += _calc_f_repeat(p2, p1, max_y, max_x, part2)
    return antinodes


def main1(filepath):
    data = read_file(filepath)
    antennas = _get_antennas(data)
    antinodes = _get_antinodes(antennas, max_y=len(data), max_x=len(data[0]))
    print(len(set(antinodes)))
    return


def main2(filepath):
    data = read_file(filepath)
    antennas = _get_antennas(data)
    antinodes = _get_antinodes(antennas, max_y=len(data), max_x=len(data[0]), part2=True)
    print(len(set(antinodes)))
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    t_start = time.time()
    main1(args.filename)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
    t_start = time.time()
    main2(args.filename)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
