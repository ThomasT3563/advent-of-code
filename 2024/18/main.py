import argparse

from heapq import heapify, heappop, heappush


def parse_file(filepath):
    return [list(map(int, l.strip().split(","))) for l in open(filepath, 'r')]


def display(grid, size):
    "for debug"
    for y in range(size[1]+1):
        line = ""
        for x in range(size[0]+1):
            if x+y*1j in grid:
                line += grid[x+y*1j]
            else:
                line += "#"
        print(line)
    return


def dijkstra(grid, source):
    "copy paste dijkstra from Day 16, remove direction and weight system. Now it's a BFS"
    min_distances = {source: 0}
    pq = [(0, str(source), source)]
    heapify(pq)
    visited = set()
    
    while pq:
        current_dist, _, current_node = heappop(pq)
        if current_node not in visited:
            visited.add(current_node)
            for neighbor in [current_node+dir for dir in [1,-1,1j,-1j] if current_node+dir in grid and grid[current_node+dir]!="#" ]:
                if neighbor not in min_distances:
                    min_distances[neighbor] = float('inf')
                # update min_dist
                dist = int(current_dist+1)  # weight uniform of +1 each step
                if dist < min_distances[neighbor]:
                    min_distances[neighbor] = dist
                    heappush(pq, (dist, str(neighbor), neighbor))

    return min_distances


def part1(data, size, n_iter=0):
    # generate empty grid
    G = {x+y*1j: "." for x in range(size[0]+1) for y in range(size[1]+1)}

    # add obstacle after N iterations
    for i in range(n_iter):
        (x,y) = data[i]
        G[x+y*1j] = "#"
    
    # run BFS on the created labyrinth
    min_dist = dijkstra(G, 0)
    
    try:
        #display(G, size)
        return min_dist[size[0]+size[1]*1j]
    except KeyError:
        raise KeyError(f"{x},{y}")  # IQ 2000



if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    
    data = parse_file(args.filename)
    
    # part 1
    print(part1(data, size=(70,70), n_iter=1024))  # size = 6x6 for test, 70x70 for input

    # part 2 : wait ~15s for the code to crash, the error message is the answer
    for i in range(1024, len(data)):
        _ = part1(data, size=(70,70), n_iter=i)