import argparse

from heapq import heapify, heappop, heappush


def parse_file(filepath):
    G = {}
    for y,l in enumerate(open(filepath, 'r')):
            for x,c in enumerate(l.strip()):
                if c!="#": G[x+y*1j] = c
                if c=="S": start = x+y*1j
                elif c=="E": end = x+y*1j
    return G, start, end


def BFS(G, start_node):
    min_dist = {start_node: 0}
    todo = [(0, str(start_node), start_node)]
    heapify(todo)
    visited = set()
    
    while todo:
        dist, _, node = heappop(todo)
        if node not in visited: 
            visited.add(node)
            for neighbor in [node+dir for dir in [1,-1,1j,-1j] if node+dir in G]:
                if neighbor not in min_dist:
                    min_dist[neighbor] = float('inf')
                n_dist = dist + 1
                if n_dist < min_dist[neighbor]:
                    min_dist[neighbor] = n_dist
                    heappush(todo, (n_dist, str(neighbor), neighbor))
    return min_dist


def abs_complex(x,y):
    return abs(x.real-y.real) + abs(x.imag-y.imag)


def main(max_cheats, min_shortcut):
    """
    takes ~15s, would be way faster with numpy
    
    evaluate manhattan distance of all points in the grid with each other
    if the distance is below 'max_cheat' threshold, it means we can cheat (or walk) through
    if abs(min_dist[A] - min_dist[B] - dist[A-B]) > min_shortcut then it is a valid shortcut
    
    the abs distance between 2 points is a triangular matrix w/ diag (N x N /2 - N)
    """
    n_shortcuts = {m:0 for m in max_cheats}
    keys = list(G.keys())
    
    for i in range(len(keys)):
        for j in range(i):
            # distance between 2 points lower than 'cheat distance'
            d = abs_complex(keys[i],keys[j])
            for max_cheat in max_cheats:
                # shortcut allows to gain at least 'min_shortcut'
                if d <= max_cheat:
                    gain = abs(min_dist[keys[i]] - min_dist[keys[j]]) - d
                    if gain >= min_shortcut:
                        n_shortcuts[max_cheat] += 1
                    
    [print(n) for n in n_shortcuts.values()]
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    
    G, start, end = parse_file(args.filename)
    min_dist = BFS(G, start)
    
    main(max_cheats=[2,20], min_shortcut=100)
