import argparse
import re


def parse_file(filepath):
    return map(int, re.findall(r'\d+', open(filepath, 'r').read()))


def combo_operand(op, A, B, C):
    combo_operators = [0,1,2,3,A,B,C,ValueError()]
    return combo_operators[op]


def main(filepath):
    pointer = 0
    output = []
    A, B, C, *program = parse_file(filepath)

    while pointer < len(program)-1:
        opcode, operand = program[pointer], program[pointer+1]

        match opcode:
            case 0: A = A >> combo_operand(operand, A, B , C)
            case 1: B = B ^ operand
            case 2: B = combo_operand(operand, A, B , C) % 8
            case 3:
                if A != 0:
                    pointer = operand
                    continue
            case 4: B = B ^ C
            case 5: output.append(str(combo_operand(operand, A, B , C) % 8))
            case 6: B = A >> combo_operand(operand, A, B , C)
            case 7: C = A >> combo_operand(operand, A, B , C)
                
        pointer += 2

    print(",".join(output))
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main(args.filename)
