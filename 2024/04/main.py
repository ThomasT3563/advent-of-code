import argparse
from collections import defaultdict


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return [line.strip() for line in data]


# https://stackoverflow.com/questions/6313308/get-all-the-diagonals-in-a-matrix-list-of-lists-in-python
def groups(data, func):
    grouping = defaultdict(list)
    for y in range(len(data)):
        for x in range(len(data[y])):
            grouping[func(x, y)].append(data[y][x])
    # removed the sorting, which is not usefull in that case
    # join each diagonal in a single string
    return ["".join(line) for line in map(grouping.get, grouping)]


def _search_word(text, word, backward=True):
    found = 0
    found += text.count(word)
    if backward:
        found += text.count(word[::-1])
    return found


def main1(filepath):
    """
    Most straightforward would be to look for each first letter and search in all directions
    from there
    But we'll try another way, we create strings of all lines of the puzzle : 
    - horizontal, vertical and both diagnonals
    and then perform a simple count of the search word
    """
    search_word = "XMAS"
    data = read_file(filepath)
    # split the original data in all directions we are looking for : 
    # both diagonals, vertical and horizontal axis
    horizontal = data.copy()
    vertical = ["".join(line) for line in list(zip(*data))]  # cc yoyo
    rights_diag = groups(data, lambda x, y: x + y)
    left_diag = groups(data, lambda x, y: x - y)
    n = [_search_word(line, search_word) for grid in [horizontal, vertical, rights_diag, left_diag]  for line in grid ]
    print(sum(n))
    return


###################################


def main2(filepath):
    """
    The first approach doesn't work anymore, we need to redo everything
    One-liner for the fun
    """
    data = read_file(filepath)
    print(sum([set([data[i-1][j-1],data[i+1][j+1]]) == set([data[i-1][j+1],data[i+1][j-1]]) ==  {'M', 'S'} for i in range(1,len(data)-1) for j in range(1,len(data[0])-1) if data[i][j] == "A"]))
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main1(args.filename)
    main2(args.filename)
