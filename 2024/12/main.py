import argparse
from itertools import product, combinations


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return [l.strip() for l in data]


def grow_seed(points, seed):
    "hint from community : for part 2 count the corners, not the vertices"
    global D, max_i, max_j
    area = 1
    sides = 0
    (i,j) = seed
    points.pop(seed)
    
    # get all valid sides of seed
    sides = []
    neighbors = []
    for di, dj in [(1,0),(-1,0),(0,1),(0,-1)]:
        if not ((0<=i+di<max_i) and (0<=j+dj<max_j) and D[i+di][j+dj]==D[i][j]):
            sides.append((di,dj))
        else:
            neighbors.append((di,dj))
    ext_corners = 0
    perimeter = len(sides)
    
    # count extern angles
    for s1, s2 in combinations(sides, r=2):
        if tuple(map(sum, zip(s1, s2))) != (0,0):  # if it's 2 consecutive vertices = corner
            ext_corners += 1
    
    # count inner angles
    inn_corners=0
    for s in sides:
        for n in neighbors:
            (ci, cj) = tuple(map(lambda x: x[0]+x[1], zip(s,n)))
            if (ci, cj)!=(0,0) and (0<=i+ci<max_i) and (0<=j+cj<max_j) and D[i+ci][j+cj]==D[i][j]:
                inn_corners += 0.5  # it will be counted twice
    
    # grow region from valid neighbor points
    for (di,dj) in neighbors:
        if (i+di, j+dj) in points:
            points, a, c, p = grow_seed(points, (i+di, j+dj))
            area += a
            ext_corners += c
            perimeter += p

    return points, area, ext_corners+inn_corners, perimeter


def main(filepath):
    global D, max_i, max_j
    D = read_file(filepath)
    max_i, max_j = len(D), len(D[0])
    part1_price = part2_price = 0
    all_points = dict.fromkeys(product(range(max_i), range(max_j)), 0)
    while len(all_points) > 0:
        all_points, area, corners, perimeter = grow_seed(all_points, seed=next(iter(all_points.keys())))
        part1_price += area*perimeter
        part2_price += area*(int(corners))
    print(f'{part1_price}\n{part2_price}')
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main(args.filename)
