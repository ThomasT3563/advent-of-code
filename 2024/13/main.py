import argparse
import re


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.read()
    return [map(int, re.findall(r'\d+', l)) for l in data.split("\n\n")]


def main(filepath, add):
    n_token = 0
    for xa, ya, xb, yb, xr, yr in read_file(filepath):
        xr += add  # for part 2
        yr += add  # for part 2
        b = (xa*yr - ya*xr) / (xa*yb - ya*xb)
        a = (xr - xb*b) / xa
        if a.is_integer() and b.is_integer():
            n_token += 3*int(a) + int(b)
    print(n_token)
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    for t in [0, 10000000000000]:
        main(args.filename, t)
