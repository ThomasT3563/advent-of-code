import argparse
import re
from more_itertools import locate


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.read()
    return data


def _mult_list_elm(lst):
    return int(lst[0]) * int(lst[1])


def main1(filepath):
    data = read_file(filepath)
    muls = re.findall(r'mul\(\d{1,3},\d{1,3}\)', data)
    # syntax : 'mul(XXX,XXX)'
    r = [_mult_list_elm(mul[4:-1].split(",")) for mul in muls]
    print(sum(r))
    return


def main2(filepath):
    data = read_file(filepath)
    matches = re.findall(r"(mul\(\d{1,3},\d{1,3}\)|do\(\)|don't\(\))", data)
    r = 0
    enabled = True
    for m in matches:
        if m == "do()":
            enabled = True
        elif m == "don't()":
            enabled = False
        elif enabled:
            r += _mult_list_elm(m[4:-1].split(","))
    print(r)
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main1(args.filename)
    main2(args.filename)
