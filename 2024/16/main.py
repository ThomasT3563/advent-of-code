import argparse

from heapq import heapify, heappop, heappush


def parse_file(filepath):
    with open(filepath, 'r') as file:
        grid = {}
        for y,l in enumerate(file.readlines()):
            for x,c in enumerate(l.strip()):
                if c in [".", "S", "E"]:
                    grid[x+y*1j] = c
                    if c == "S":
                        start = x+y*1j
                    elif c == "E":
                        end = x+y*1j
    return grid, start, end, (x,y)


def display(grid, size):
    "for debug"
    for y in range(size[1]+1):
        line = ""
        for x in range(size[0]+1):
            if x+y*1j in grid:
                line += grid[x+y*1j]
            else:
                line += "#"
        print(line)
    return


def get_neighbors(grid, pos, dir):
    neighbors = []
    for d, s in [(dir, 1), (dir*1j, 1001), (dir/1j, 1001)]:  # forward, left, right
        if pos+d in grid:
            neighbors.append((pos+d, d, s))
    return neighbors


def dijkstra(grid, source, start_dir):
    "adaptation of dijkstra to index nodes during exploration"
    min_distances = {source: 0}
    pq = [(0, str(source), source, start_dir)]
    heapify(pq)
    visited = set()
    
    while pq:
        current_dist, _, current_node, current_dir = heappop(pq)
        if current_node not in visited:
            visited.add(current_node)
            for (neighbor, dir, weight) in get_neighbors(grid, current_node, current_dir):
                if neighbor not in min_distances:
                    min_distances[neighbor] = float('inf')
                # update min_dist
                dist = int(current_dist+weight)
                if dist < min_distances[neighbor]:
                    min_distances[neighbor] = dist
                    heappush(pq, (dist, str(neighbor), neighbor, dir))

    return min_distances


def main(filepath):
    start_dir = 1
    grid, start, end, size = parse_file(filepath)
    min_distances = dijkstra(grid, start, start_dir)
    #display(grid, size)
    print(min_distances[end])
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main(args.filename)
