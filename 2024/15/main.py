import argparse
from functools import reduce
import re

ROBOT="@"
BOXES="O"
WALL="#"
SPACE="."


def read_file(filepath):
    with open(filepath, 'r') as file:
        [map, dir] = file.read().split("\n\n")
    G = {x+y*1j: c for y,l in enumerate(map.split("\n")) for x,c in enumerate(l.strip())}
    return G, "".join([l.strip() for l in dir])


def _display(G):
    "debug purpose"
    print("\ngrid display:\n")
    for j in range(50):
        line = []
        if 0+j*1j not in G:
            break
        for i in range(50):
            if i+j*1j in G:
                line.append(G[i+j*1j])
            else:  # end of line
                break
        print("".join(line))
        
    return


def move_if_elligible(G, r, m):
    # move robot without collision
    if r+m in G and G[r+m]==SPACE:
        G[r], G[r+m] = SPACE, ROBOT
        return G, r+m
    
    # list elligible objects that can be moved
    line = []
    for i in range(1, 50):
        if r+i*m in G:
            if G[r+i*m] in BOXES:
                line.append(r+i*m)
            elif G[r+i*m]==SPACE:
                line.append(r+i*m)
                break
            else:
                break
    
    # move objects
    if len(line)>0 and G[line[-1]] == SPACE:  # can move
        G[r] = SPACE
        G[line[0]] = ROBOT
        for e in line[1:]:
            G[e] = BOXES
        return G, r+m
    else:
        return G, r


def main1(filepath, display=False):
    G, dirs = read_file(filepath)
    
    # initial robot coordinates
    r = [k for k,v in G.items() if v==ROBOT][0]
    
    # iterates move instructions
    for dir in dirs:
        m = 1*(dir==">") -1*(dir=="<") -1j*(dir=="^") + 1j*(dir=="v")
        G, r = move_if_elligible(G, r, m)
        
    if display:
            _display(G)

    # result GPS score
    print(sum(map(lambda x : 100*int(x.imag) + int(x.real), [k for k,v in G.items() if v==BOXES])))
    return


def main2(filepath):
    print("Hello world!")
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main1(args.filename, display=False)
    #main2(args.filename)
