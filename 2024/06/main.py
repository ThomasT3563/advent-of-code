import argparse
from copy import deepcopy
from itertools import cycle
import time

import sys
sys.setrecursionlimit(6000)


RTURN = [(-1,0),(0,1),(1,0),(0,-1)]


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return [line.strip() for line in data]


def _get_map(data):
    global RTURN_CYCLE
    RTURN_CYCLE = cycle(RTURN)  # starts at (-1,0)
    obstacles = []
    for y, line in enumerate(data):
        for x, elm in enumerate(line):
            if elm == ".":
                continue
            elif elm == "#":
                obstacles.append((y,x))
            else:
                vector = (y,x) + next(RTURN_CYCLE)  # (y, x, dy, dx)
    return obstacles, vector, len(data), len(data[0])


def _move(data, obstacles, vector, max_y, max_x):
    next_pos = (vector[0]+vector[2], vector[1]+vector[3])
    if next_pos in obstacles:
        vector = vector[:2] + next(RTURN_CYCLE)
    elif not (0 <= next_pos[0] < max_y) or not (0 <= next_pos[1] < max_x):
        return False  # out of map
    else:
        vector = next_pos + vector[2:]
    if vector in known_vectors:
        return True  # looping
    known_vectors.add(vector)
    looping = _move(data, obstacles, vector, max_y, max_x)
    return looping


def _resolve(data):
    obstacles, vector, max_y, max_x = _get_map(data)
    global known_vectors
    known_vectors = set([vector])
    looping = _move(data, obstacles, vector, max_y, max_x)  # updates known_vectors
    return known_vectors, looping


def main1(filepath):
    data = read_file(filepath)
    known_vectors, _ = _resolve(data)
    print(len(set([(pos[0], pos[1]) for pos in known_vectors])))
    return


def main2(filepath):
    data = read_file(filepath)
    # initial run
    obstacles, vector, max_y, max_x = _get_map(data)
    global known_vectors
    known_vectors = set([vector])
    looping = _move(data, obstacles, vector, max_y, max_x) 
    positions = set([(pos[0], pos[1]) for pos in known_vectors])
    positions.remove(vector[:2])  # ignore start point
    # add an obstacle at each position and check if it loops
    n_loop_obs = 0
    for i, (y, x) in enumerate(positions):
        it_data = deepcopy(data)
        it_data[y] = "".join(["#" if i==x else l for i, l in enumerate(it_data[y])])
        known_vectors, looping = _resolve(it_data)
        #print(f"test position n°{i+1}:{(y, x)}: looping={looping} / n={len(set([(pos[0], pos[1]) for pos in known_vectors]))}")
        n_loop_obs += looping
        
    print(n_loop_obs)
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    t_start = time.time()
    main1(args.filename)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
    t_start = time.time()
    main2(args.filename)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
