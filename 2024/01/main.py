import argparse


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return data


def common(filepath):
    data = read_file(filepath)
    left_list = []
    right_list = []
    for line in data:
        l, r = line.split("   ")
        left_list.append(int(l))
        right_list.append(int(r))
    return left_list, right_list


def main(filepath):
    left_list, right_list = common(filepath)
    left_list.sort()
    right_list.sort()
    n = [abs(l-r) for l, r in zip(left_list, right_list)]
    print(sum(n))
    return


def main2(filepath):
    left_list, right_list = common(filepath)
    n = [l * right_list.count(l) for l in left_list]
    print(sum(n))
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main(args.filename)
    main2(args.filename)
