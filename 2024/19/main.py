import argparse
import re

from functools import cache


def parse_file(filepath):
    pat, *designs = open(filepath)
    return re.findall(r'\w+', pat), [d.strip() for d in designs[1:]]


@cache
def count_arrangments(text):
    # memoization to avoid repetition of patterns, which happens a lot
    if text: 
        return sum([count_arrangments(text[len(p):]) for p in patterns if text.startswith(p)])
    else:
        return 1


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    
    patterns, designs = parse_file(args.filename)
    sols = [count_arrangments(d) for d in designs]  # runs in ~1s
    # part 1: number of time solutions are found for a design
    # part 2: total amount of solutions
    print(sum([s>0 for s in sols]), sum(sols), sep='\n')