import argparse
from functools import cache
import re
import time


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.read()
    return [int(i) for i in re.findall(r'\d+', data)]


@cache
def blink(x):
    if x==0:
        return [1]
    s = str(x)
    l = len(s)
    if l % 2 == 0:
        return [int(s[l//2:]), int(s[:l//2])]
    return [x*2024]


###### don't do that, list is too heavy #######
@cache
def DFS_list(x, depth, max_depth):
    res = []
    if depth+1 == max_depth:
        return [x]
    bdata = blink(x)
    for x in bdata:
        res += DFS_list(x, depth+1, max_depth)
    return res
###############################################


@cache
def DFS_count(x, depth, max_depth):
    res = 0
    if depth+1 == max_depth:
        return 1  # returns count instead of list
    bdata = blink(x)
    for x in bdata:
        res += DFS_count(x, depth+1, max_depth)
    return res


def main(filepath, max_depth=1):
    data = read_file(filepath)
    res = 0
    for x in data:
        res += DFS_count(x, 0, max_depth+1)
    print(res)
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    t_start = time.time()
    main(args.filename, max_depth=25)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
    t_start = time.time()
    main(args.filename, max_depth=75)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
