import argparse
import time


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return "".join(line for line in data)


def _get_data(data):
    rules = {}
    updates = []
    s1, s2 = data.split("\n\n")
    rules = {}
    for line in s1.split("\n"):
        v1, v2 = line.split("|")
        if v1 not in rules:
            rules[v1] = []
        if v2 not in rules:
            rules[v2] = []
        rules[v1].append((v1,v2))
        rules[v2].append((v1,v2))
    updates = [line.split(",") for line in s2.split("\n")]
    return rules, updates


def main1(filepath):
    data = read_file(filepath)
    rules, updates = _get_data(data)
    valid_n = 0
    for update in updates:
        valid = True
        for n in update:
            for rule in rules[n]:
                # if both element of the rule are present in the update
                if set(rule) <= set(update):
                    # split update into section before and after n
                    u1 = update[:update.index(n)]
                    if update.index(n) <= len(update):
                        u2 = update[update.index(n)+1:]
                    else:
                        u2 = []
                    # check rule
                    if ((rule[0] == n) and (rule[1] not in u2)) or ((rule[1] == n) and (rule[0] not in u1)):
                        valid = False
                        break
        if valid:
            valid_n += int(update[len(update) // 2])
    print(valid_n)
    return


def _fix_update(update, rules, max_loop=50):
    new_update = update.copy()
    valid = [False] * len(update)
    n_loop = 0
    while not all(valid) and n_loop < max_loop:
        n_loop += 1
        update = new_update.copy()
        valid = [False] * len(update)
        for i, n in enumerate(update):
            all_correct = True
            for rule in rules[n]:
                if set(rule) <= set(update):
                    u1 = update[:update.index(n)]
                    u2 = []
                    if update.index(n) <= len(update):
                        u2 = update[update.index(n)+1:]
                    if rule[0] == n:
                        if rule[1] not in u2:
                            all_correct = False
                            # fix
                            u1.pop(u1.index(rule[1]))
                            new_update = u1 + [n, rule[1]] + u2
                            break # skip other rules if we reached this one
                    else:
                        if rule[0] not in u1:
                            all_correct = False
                            # fix
                            u2.pop(u2.index(rule[0]))
                            new_update = u1 + [rule[0], n] + u2
                            break  # skip other rules if we reached this one
            if all_correct:
                valid[i] = True
            else:
                # skip checking the update if we already fixed a part
                # saves half running time, if we fix the 97 for "97|75", no
                # need to duplicate it with the 75
                break  
    return update


def main2(filepath):
    data = read_file(filepath)
    rules, updates = _get_data(data)
    valid_n = 0
    for update in updates:
        valid = True
        for n in update:
            for rule in rules[n]:
                # if both element of the rule are present in the update
                if set(rule) <= set(update):
                    # split update into section before and after n
                    u1 = update[:update.index(n)]
                    if update.index(n) <= len(update):
                        u2 = update[update.index(n)+1:]
                    else:
                        u2 = []
                    # check rule
                    if ((rule[0] == n) and (rule[1] not in u2)) or ((rule[1] == n) and (rule[0] not in u1)):
                        valid = False
                        break
        if not valid:
            fixed_update = _fix_update(update, rules)
            valid_n += int(fixed_update[len(fixed_update) // 2])
    print(valid_n)
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    t_start = time.time()
    main1(args.filename)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
    t_start = time.time()
    main2(args.filename)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
