import argparse
import re
import time


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.read().strip() + '0'  # in case of off number of digits
    return [[int(t[0]), int(t[1]), id] for id, t in enumerate(re.findall(r'..', data))]


def main1(filepath):
    """
    Build list of all blocks, get the indexes of free space and permute
    free space and numbers
    """
    data = read_file(filepath)
    n_free_space = sum([t[1] for t in data[:-1]])
    
    # build blocks
    blocks = []
    for i, (n, f, _) in enumerate(data):
        blocks += [i]*n + ["."]*f
    
    # indexing
    idx_free_spaces = []
    idx_nbs = []
    idx = 0
    for (n, f, _) in data[:-1]:  # ignore last element
        idx_nbs += [idx+j for j in range(n)]
        idx_free_spaces += [idx+n+k for k in range(f)]
        idx += n + f
    idx_nbs += [idx+j for j in range(data[-1][0])]  # add last element
    
    # permute elements
    for idx_f, idx_n in zip(idx_free_spaces, idx_nbs[:-(len(idx_free_spaces)+1):-1]):
        # stops once all completed
        if idx_f >= (len(blocks)-n_free_space):
            break
        blocks[idx_f], blocks[idx_n] = blocks[idx_n], blocks[idx_f]
        
    # checksum
    print(sum([i*n for i, n in enumerate(blocks) if n != "."]))
    return


def main2(filepath):
    """
    Completely different solution for part 2 :
    We work directly with the inputs, no convertion in blocks
    Each file is stored as (nb_of_element, nb_of_free_spaces, id_of_block)
    and we swap them such as:
    - (2,4,0), ..., (2,0,99)
    - (2,0,0), (2,2,99), ..., (0,2,99)
    """
    data = read_file(filepath)
    
    for f_to_move in data[::-1]:
        for j, f_to_receive in enumerate(data[:data.index(f_to_move)]):
            if f_to_receive[1] >= f_to_move[0]:  # can swap
                data.insert(j+1, [f_to_move[0], f_to_receive[1]-f_to_move[0], f_to_move[2]])
                f_to_receive[1] = 0
                f_to_move[1] += f_to_move[0]
                f_to_move[0] = 0
                break
    
    # build blocks for checksum
    blocks = []
    for (n, f, id) in data:
        blocks += [id]*n + ["."]*f
    print(sum([i*n for i, n in enumerate(blocks) if n != "."]))
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    t_start = time.time()
    main1(args.filename)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
    t_start = time.time()
    main2(args.filename)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
