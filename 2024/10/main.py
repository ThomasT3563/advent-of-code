import argparse
import time


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
    return [l.strip() for l in data]


def _get_trail_routes(p, G):
    p_val = int(G[p[0]][p[1]])
    dest = [(p[0]+1, p[1]), (p[0], p[1]+1), (p[0]-1, p[1]), (p[0], p[1]-1)]
    for d in dest:
        if (0 <= d[0] < len(G)) and (0 <= d[1] < len(G[0])) and int(G[d[0]][d[1]])==p_val+1:
            valid_routes[p_val+1].append(d)
            _ = _get_trail_routes(d, G)
    return


def main(filepath):
    G = read_file(filepath)
    trailheads = [(j,i) for j, y in enumerate(G) for i, x in enumerate(y) if x=="0"]
    score_part1 = 0
    score_part2 = 0
    for t in trailheads:
        global valid_routes
        valid_routes = [[] for _ in range(10)]
        _get_trail_routes(t, G)  # updates valid_routes
        score_part1 += len(set(valid_routes[-1]))  # unique final point
        score_part2 += len(valid_routes[-1])  # all paths to final point
    print(score_part1, score_part2)
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    t_start = time.time()
    main(args.filename)
    print(f"time elapsed: {time.time()-t_start:.2f} seconds.")
    t_start = time.time()
