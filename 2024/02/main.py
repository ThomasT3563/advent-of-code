import argparse


def read_file(filepath):
    with open(filepath, 'r') as file:
        data = file.readlines()
        reports = [l.strip().split(" ") for l in data]
    return reports


def _get_safe_status(levels):
    sub = [int(x)-int(y) for x,y in zip(levels[:-1], levels[1:])]
    c1 = all([abs(el) <=3 and abs(el) >= 1 for el in sub])
    c2 = all([el > 0 for el in sub]) or all([el < 0 for el in sub])
    return c1 and c2


def main1(filepath):
    reports = read_file(filepath)
    safes = [_get_safe_status(levels) for levels in reports]
    print(sum(safes))
    return


def main2(filepath):
    reports = read_file(filepath)
    safes = []
    for levels in reports:
        s = False
        tol_levels = [levels] + [levels[:i] + levels[i+1:] for i in range(len(levels))]
        for slvl in tol_levels:
            if _get_safe_status(slvl):
                s = True
                break
        safes.append(s)
    print(sum(safes))
    return


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main1(args.filename)
    main2(args.filename)
